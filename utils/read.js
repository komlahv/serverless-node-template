const AWS = require("aws-sdk");
let awsConfig = {
  "region": "us-east-1",
  "endpoint": "http://dynamodb.us-east-1.amazonaws.com",
  // "accessKeyId": "...", "secretAccessKey": "....."
};
AWS.config.update(awsConfig);

const documentClient = new AWS.DynamoDB.DocumentClient({ apiVersion: "2012-10-08" });
const fetchOneByKey = async () => {

  let data;
  const params = {
    TableName: "Users",
    Key: {
      id: "7ff24e19-2ee5-4e8a-8f71-bd75b367b745"
    }
  }
  try {
    data = await documentClient.get(params).promise();
    console.log(data);
  } catch (e) { console.log(e); }
}

// fetchOneByKey();

module.exports = { fetchOneByKey }
