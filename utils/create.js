const { v4: uuidv4 } = require('uuid');
const AWS = require("aws-sdk");
let awsConfig = {
  "region": "us-east-1",
  "endpoint": "http://dynamodb.us-east-1.amazonaws.com",
  // "accessKeyId": "...", "secretAccessKey": "....."
};
AWS.config.update(awsConfig);

const documentClient = new AWS.DynamoDB.DocumentClient({ apiVersion: "2012-10-08" });

const create = async () => {
  let generatedID = uuidv4();
  let data;
  const params = {
    TableName: "Users",
    Item: {
      id: generatedID,
      name: "Tony",
      car: "Benz"
    }
  };

  try {
    data = await documentClient.put(params).promise();
    console.log(data);
  } catch (e) { console.log(e); }
}

// create();

module.exports = { create }
