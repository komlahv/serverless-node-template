const AWS = require("aws-sdk");
let awsConfig = {
  "region": "us-east-1",
  "endpoint": "http://dynamodb.us-east-1.amazonaws.com",
  // "accessKeyId": "...", "secretAccessKey": "....."
};
AWS.config.update(awsConfig);

const documentClient = new AWS.DynamoDB.DocumentClient({ apiVersion: "2012-10-08" });

const update = async () => {

  let data;
  const params = {
    TableName: "Users",
    Key: {
      id: "12345"
    },
    UpdateExpression: "set updated_by = :byUser, is_deleted = :boolValue",
    ExpressionAttributeValues: {
      ":byUser": "updateUser",
      ":boolValue": true
    },
    ReturnValues: "UPDATED_NEW"

  };
  try {
    data = await documentClient.update(params).promise();
    console.log(data);
  } catch (e) { console.log(e); }
}

// update();

module.exports = { update }