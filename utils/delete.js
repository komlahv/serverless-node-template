const AWS = require("aws-sdk");
let awsConfig = {
  "region": "us-east-1",
  "endpoint": "http://dynamodb.us-east-1.amazonaws.com",
  // "accessKeyId": "...", "secretAccessKey": "....."
};
AWS.config.update(awsConfig);

const documentClient = new AWS.DynamoDB.DocumentClient({ apiVersion: "2012-10-08" });

const remove = async () => {

  const params = {
    TableName: "Users",
    Key: {
      id: "12348"
    }
  }
  try {
    data = await documentClient.delete(params).promise();
    console.log(data);
  } catch (e) { console.log(e); }
}

// remove();

module.exports = { remove }