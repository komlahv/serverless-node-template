'use strict';
const AWS = require('aws-sdk');

module.exports.hello = async event => {

  console.log(event);
  // const ddb = AWS.DynamoDB({ apiVersion: "2012-10-08" });
  // const params = {
  //   TableName: "Users",
  //   Key: {
  //     id: {
  //       S: "12345"
  //     }
  //   }
  // }

  // ddb.getItem(params, (err, data) => {
  //   if (err) {
  //     console.log(err);
  //   }
  //   console.log(data);
  // })

  //use AWS Document client so you can use regular JSON syntax
  // const documentClient = new AWS.DynamoDB.DocumentClient({ apiVersion: "2012-10-08" })
  // let data;
  // const params = {
  //   TableName: "Users",
  //   Key: {
  //     id: "12345"
  //   }
  // }

  // try {
  //   data = await documentClient.get(params).promise();
  // } catch (e) { console.log(e); }

  // const puTparams = {
  //   TableName: "Users",
  //   Item: {
  //     id: "12345",
  //     name: "Sandy"
  //   }
  // }

  // try {
  //   data = await documentClient.put(puTparams).promise();
  // } catch (e) { console.log(e); }

  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go Serverless v1.0! Awesome, Your function executed successfully!',
        input: event,
        dbResponse: data
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
